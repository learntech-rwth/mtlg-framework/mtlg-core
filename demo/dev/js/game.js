// see https://mtlg-framework.gitlab.io/api/MTLG.html for API
// see https://gitlab.com/mtlg-framework/mtlg-gameframe/wikis/mtlg/basic for the introduction in the wiki


// expose MTLG and createjs to the global namespace for debugging in the web developer console
// this is done in the framework in a later version
window.MTLG = MTLG;
window.window.createjs = createjs;

// start the framework as soon as the html page is loaded
document.addEventListener("DOMContentLoaded", MTLG.init);

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

// load translations
require('../lang/lang.js');

// register the function that will define the playable levels
MTLG.addGameInit(initGame);

// keep track of the current level
var currentLevel = "first_level";

// setup gamelevels
function initGame() {
  // At least one level is required. The second argument is a callback that tells whether this level shall be started.
  MTLG.lc.registerLevel(firstLevel, () => currentLevel == "first_level");

  // register other demo level
  MTLG.lc.registerLevel(assetLevel, () => currentLevel == "asset_level");
  MTLG.lc.registerLevel(langLevel, () => currentLevel == "lang_level");
  MTLG.lc.registerLevel(lcLevel, () => currentLevel == "lc_level");
}

function firstLevel() {
  // Access the root container to which all UI elements will be added.
  var stage = MTLG.getStageContainer();

  // Create a text and add it to the stage.
  var button1 = createButton("clearBackground");
  button1.x = 20;
  button1.y = 100
  stage.addChild(button1);

  // Add an event handler for clicking on the text.
  button1.on("click", () => {
    // reset background
    MTLG.clearBackground();
  });

  // Add some more texts
  var button2 = createButton("setBackgroundImage");
  button2.x = 20;
  button2.y = 180;
  stage.addChild(button2);
  button2.on("click", () => {
    MTLG.setBackgroundImage("rorschach.png");
  });

  var button3 = createButton("setBackgroundImageFill");
  button3.x = 20;
  button3.y = 260;
  stage.addChild(button3);
  button3.on("click", () => {
    MTLG.setBackgroundImageFill("rorschach.png");
  });

  var button4 = createButton("setBackgroundColor");
  button4.x = 20;
  button4.y = 340;
  stage.addChild(button4);
  button4.on("click", () => {
    // Set the background to purple.
    MTLG.setBackgroundColor("purple");
  });

  var button5 = createButton("getBackgroundStage");
  button5.x = 20;
  button5.y = 420;
  stage.addChild(button5);
  button5.on("click", () => {
    // remove all children from the background stage
    MTLG.getBackgroundStage().removeAllChildren();
    // repaint the background stage
    MTLG.getBackgroundStage().update();
  });

  var button6 = createButton("setBackgroundContainer");
  button6.x = 20;
  button6.y = 500;
  stage.addChild(button6);
  button6.on("click", () => {
    var container = new createjs.Container();
    container.addChild( new createjs.Text("A", "1000px Arial", "blue"), new createjs.Text("B", "800px Arial", "red"));
    MTLG.setBackgroundContainer(container);
  });

  var button7 = createButton("clearStage");
  button7.x = 20;
  button7.y = 580;
  stage.addChild(button7);
  button7.on("click", () => {
    // remove all children from the scene
    MTLG.clearStage();
  });

  var button8 = createButton("see console (press F12 to open)");
  button8.x = 20;
  button8.y = 660;
  stage.addChild(button8);
  button8.on("click", () => {
    // log some values
    console.log("----------------------------")
    console.log("getOptions", MTLG.getOptions());
    console.log("getScaleFactor", MTLG.getScaleFactor());
    console.log("getSettings", MTLG.getSettings());
  });

  var button9 = createButton("Asset demo");
  button9.x = 20;
  button9.y = 740;
  stage.addChild(button9);
  button9.on("click", () => {
    currentLevel = "asset_level";
    MTLG.lc.levelFinished();
  });

  var button10 = createButton("Language demo");
  button10.x = 20;
  button10.y = 820;
  stage.addChild(button10);
  button10.on("click", () => {
    currentLevel = "lang_level";
    MTLG.lc.levelFinished();
  });

  var button11 = createButton("Lifecycle demo");
  button11.x = 20;
  button11.y = 820;
  stage.addChild(button11);
  button11.on("click", () => {
    currentLevel = "lc_level";
    MTLG.lc.levelFinished();
  });
}

// Helper function to create a button. See https://mtlg-framework.gitlab.io/api/MTLG.utils.uiElements.html for simpler button creation.
function createButton(text) {
  var container = new createjs.Container();
  var rectangle = new createjs.Shape();
  rectangle.graphics.beginFill("white").drawRect(0, 0, 820, 70);
  var textElement = new createjs.Text(text, "50px Arial");
  textElement.x = 10;
  textElement.y = 10;
  textElement.color = "blue";
  container.addChild(rectangle, textElement);
  return container;
}

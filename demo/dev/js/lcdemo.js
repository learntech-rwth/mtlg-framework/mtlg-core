
function lcLevel() {
  // Access the root container to which all UI elements will be added.
  var stage = MTLG.getStageContainer();

  function gameStateDemoLevel(gameState) {
    console.log(gameState)
    MTLG.lc.levelFinished(null)
  }
  MTLG.lc.registerLevel(gameStateDemoLevel, gameState => gameState && gameState.answer == 42 ? 2 : 0);

  var button1 = createButton("getLevels (see console)").set({x:20, y:100});
  button1.on("click", () => {
    console.log(...MTLG.lc.getLevels());
  });

  var button2 = createButton("hasMenu (see console)").set({x:20, y:180});
  button2.on("click", () => {
    console.log(MTLG.lc.hasMenu());
  });

  var button3 = createButton("getMenu (see console)").set({x:20, y:260});
  button3.on("click", () => {
    console.log(MTLG.lc.getMenu());
  });

  var button4 = createButton("goToLogin").set({x:20, y:340});
  button4.on("click", () => {
    MTLG.lc.goToLogin();
  });

  var button5 = createButton("goToMenu").set({x:20, y:420});
  button5.on("click", () => {
    MTLG.lc.goToMenu();
  });

  var button6 = createButton("levelFinished").set({x:20, y:500});
  button6.on("click", () => {
    MTLG.lc.levelFinished({'answer':42});
  });

  var button7 = createButton("registerLevel").set({x:20, y:580});
  button7.on("click", () => {
    MTLG.lc.registerLevel(function someLevel(){}, function someCondition(gameState) {return 0;}, null, [/.*\.mp3/, /img\/*/]);
  });

  var button8 = createButton("registerMenu").set({x:20, y:660});
  button8.on("click", () => {
    MTLG.lc.registerMenu(function someLevel(){}, null, [/.*\.mp3/, /img\/*/]);
  });

  var button9 = createButton("resizeStage").set({x:20, y:740});
  button9.on("click", () => {
    MTLG.lc.resizeStage();
  });

  var button10 = createButton("startGame").set({x:20, y:820});
  button10.on("click", () => {
    MTLG.lc.startGame();
  });

  stage.addChild(button1, button2, button3, button4, button5, button6, button7, button8, button9, button10);
}

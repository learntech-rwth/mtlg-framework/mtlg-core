/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-08-22T16:28:48+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-22T15:58:50+01:00
 */

"use strict";

/**
 * This is the life cycle demo module.
 * It handles level progression and menu creation, as well as the game progress.
 * @namespace lc
 * @memberof MTLG
 */
var levels; //Saved levels: Object {entry : function(gameState), condition : function(gameState), lastPass : int}
var menu; //Menu if present: function()
var options; //manifest options
var _currentLevel; // instance of the current level or menu
var assets;

/**
 * Init function called in MTLG.init to set up the variables
 * @param {object} opt - the options currently available to the framework
 * @memberof MTLG.lc#
 */
var init = function(opt) {
  levels = [];
  options = opt;
  assets = MTLG.assets;
}


/**
 * Description
 *
 * @param {function} entryCallback     The function that is called to create the new level
 * @param {function} conditionCallback The function that is called to evaluate if this level should be started next. Should return a number in [0,1] depending on the game state passed as a parameter.
 * @param {function} resizeCallback    The function that is called to resize the level
 * @memberof MTLG.lc#
 *
 */
function registerLevel(entryCallback, conditionCallback, resizeCallback, assets) {
  var resizeCallback = resizeCallback || function() {
    MTLG.log("lc", "No resizeCallback defined.");
  };
  if (!entryCallback || !conditionCallback) {
    MTLG.warn("lc", "MTLG.lc.registerLevel expects at least two arguments: The entry Callback and a condition Callback.");
    return;
  }
  levels.push({
    entry: entryCallback,
    condition: conditionCallback,
    lastPass: 0,
    resize: resizeCallback,
    assets
  });
}


/**
 * Call this function to get to the next level.
 * Tries to open menu if no levels are registered.
 * @param {object} gameState - the current gameState, that is passed to the conditionCallbacks to evaluate the next level.
 * @memberof MTLG.lc#
 */
var levelFinished = function(gameState) {
  //Call the condition function for all levels to find how suitable they are
  //Also find the level with the highest lastPass value, this is the one we will go to.
  //If all values are 0 go to main menu.
  var max = 0;
  var maxI = -1;
  for (var i = 0; i < levels.length; i++) {
    try {
      levels[i].lastPass = (levels[i].condition)(gameState) || 0;
      if (levels[i].lastPass > max) {
        max = levels[i].lastPass;
        maxI = i;
      }
    } catch (err) {
      MTLG.log("lc", "Error calling conditionCallback function: ");
      MTLG.log("lc", err);
      MTLG.log("lc", levels[i]);
    }
  }
  //Check if we found a level
  if (maxI >= 0 && maxI < levels.length) {
    try {
      progressBar = null;
      assets.load(levels[maxI].assets, () => {
        MTLG.lc.nextScene(levels[maxI].entry, gameState);
      }, loadingAnimation);
      _currentLevel = levels[maxI]; // set the current level
    } catch (err) {
      MTLG.log("lc", "Error calling entry callback: ");
      MTLG.log("lc", err);
      MTLG.log("lc", levels[maxI]);
      goToMenu(); //Something went wrong, go to menu
    }
  } else { //Go to menu
    goToMenu();
  }
}

/**
 * Draws the next scene.
 * This function can be hooked to implement scene transitions.
 * @param {function} entry - the scene's draw function.
 * @param {object} gameState - the current gameState which is passed to the level entry function.
 * @memberof MTLG.lc#
 */
function nextScene(entry, gameState) {
  MTLG.clearStage();
  _resetScaling();
  entry(gameState);
  resizeStage();
}


/**
 * Registers a main menu
 * @param {function} entryCallback  The function that creates the menu
 * @param {function} resizeCallback The function that resizes the menu
 *
 * @memberof MTLG.lc#
 */
function registerMenu(entryCallback, resizeMainMenu, assets) {
  var resize = resizeMainMenu || function() {
    MTLG.log("lc", "No resizeCallback defined.");
  };
  if (!entryCallback) {
    MTLG.log("lc", "Registration of a menu requires the entry callback as parameter!");
    return;
  } else {
    menu = entryCallback;
    menu.resize = resize;
    menu.assets = assets;
  }
}

/**
 * returns an array of the entry functions of all levels registered. Does not include the main menu.
 * @return {array} - all levels but not the main menu
 * @memberof MTLG.lc#
 */
var getLevels = function() {
  return levels;
}
/**
 * returns the entry function of the main menu.
 * @return {function} The entry function of the main menu.
 * @memberof MTLG.lc#
 */
var getMenu = function() {
  return menu;
}
/**
 * returns true if a menu was registered.
 * @return {bool} - true is menu exists, false otherwise.
 * @memberof MTLG.lc#
 */
var hasMenu = function() {
  return (menu != undefined);
}

/**
 * calls the main menu entry function.
 * @memberof MTLG.lc#
 */
var goToMenu = function() {
  try {
    _currentLevel = menu;
    progressBar = null;
    assets.load(menu.assets, () => {
      MTLG.clearStage();
      _resetScaling();
      menu();
      resizeStage();
    }, loadingAnimation);
  } catch (err) {
    MTLG.log("lc", "Unable to create menu");
    MTLG.log("lc", err);
    MTLG.log("lc", menu);
  }
}

var progressShape;
var progressBar;
function drawCCircle(graphics, color, x, y, outerRadius, innerRadius, fromDegree, toDegree) {
  var strokeWidth = outerRadius - innerRadius,
      offset = strokeWidth / 2;

  graphics
      .setStrokeStyle(strokeWidth)
      .beginStroke(color)
      .arc(x, y, outerRadius, _d2r(fromDegree), _d2r(toDegree))
      .endFill();
}
function _d2r(degree) {
  return (degree / 360) * 2 * Math.PI;
}
var loadingAnimation = function ({progress, totalProgress}) {
  var stage = MTLG.getStageContainer();
  if (progressBar == null) {
    progressBar = new createjs.Container();

    progressShape = new createjs.Shape();
    drawCCircle(progressShape.graphics, 'white', 0, 0, 300, 280, 0, 0);

    progressBar.addChild(progressShape);
    progressBar.rotation = 90;
    progressBar.x = MTLG.getOptions().width / 2;
    progressBar.y = MTLG.getOptions().height / 2;

    var text = new createjs.Text("MTLG", "150px Arial", "#ffffff");
    var bounds = text.getBounds();
    text.regX = bounds.width / 2;
    text.regY = bounds.height / 2;
    text.rotation = - 90;
    progressBar.addChild(text);
    MTLG.getStageContainer().addChild(progressBar);
  }
  var degree = progress * 360;
  var alpha;
  if(progress <= 0.5) {
    alpha = progress * 100 / 50;
  }
  else {
    alpha = (1 - progress) * 100 / 50;
  }
  progressBar.alpha = alpha;
  progressShape.graphics.clear();
  drawCCircle(progressShape.graphics, 'white', 0, 0, 300, 280, 0, degree);
};

/**
 * Starts the game.
 * If the game has its own main menu go there.
 * Else start the game with the login screen.
 * @memberof MTLG.lc#
 */
var startGame = function() {
  MTLG.clearStage();
  _resetScaling();
  resizeStage();
  if (menu) {
    goToMenu();
  } else {
    goToLogin();
  }
}

/**
 * use this function to trigger the login process of the MTLG framework.
 * New users will be added to the framework and can be retrieved using the corresponding MTLG function.
 * See the wiki or API for more information on the login process.
 * @memberof MTLG.lc#
 */
var goToLogin = function() {
  MTLG.clearStage();
  _resetScaling();
  MTLG.erstelleLoginScreen(MTLG.getStage(), options.loginMode); //3rd param: Methode. Standard: Messe
  resizeStage();
}

var _scaleOld = 1;
var _scaleOldX = 1;
var _scaleOldY = 1;

function _resetScaling() {
  _scaleOld = 1;
  _scaleOldX = 1;
  _scaleOldY = 1;
}

/**
 * Triggers a stage resize
 *
 * @return {number} the new scale factor
 * @memberof MTLG.lc#
 */
function resizeStage() {

  let i, l;
  let children = MTLG.getStage().children;
  let scaleX = window.innerWidth / MTLG.getOptions().width;
  let scaleY = window.innerHeight / MTLG.getOptions().height;
  let scaleFactor = Math.min(scaleX, scaleY);
  let bgStage = MTLG.getBackgroundStage();


  if (MTLG.getOptions().responsive) {

    if (MTLG.getOptions().width < screen.width || MTLG.getOptions().height < screen.height) {
      MTLG.loadOptions({
        "width": screen.width, //game width in pixels
        "height": screen.height, //game height in pixels
      });
    }

    scaleX = window.innerWidth / screen.width;
    scaleY = window.innerHeight / screen.height;

    for (i = 0, l = children.length; i < l; i++) {
      // children smaller
      children[i].scaleX = scaleX;
      children[i].scaleY = scaleY;
      // put them on the new position
      children[i].x = children[i].x / _scaleOldX * scaleX;
      children[i].y = children[i].y / _scaleOldY * scaleY;
    }
    // scale Background
    bgStage.scaleX = scaleX;
    bgStage.scaleY = scaleY;
    bgStage.update();

    try {
      if (MTLG.menu.isInitialized()) {
        MTLG.menu.resizeResponsive(scaleX, scaleY);
      }
    } catch (err) {
      MTLG.log("lc", "MTLG.menu scaling error: " + err.message);
    }

    // safe old values
    _scaleOldX = scaleX;
    _scaleOldY = scaleY;

  } else {
    for (i = 0, l = children.length; i < l; i++) {
      // children smaller
      children[i].scaleX = children[i].scaleX / _scaleOld * scaleFactor;
      children[i].scaleY = children[i].scaleY / _scaleOld * scaleFactor;
      // put them on the new position
      children[i].x = children[i].x / _scaleOld * scaleFactor;
      children[i].y = children[i].y / _scaleOld * scaleFactor;
    }

    if (scaleX < scaleY) {
      MTLG.getStage().canvas.width = bgStage.canvas.width = window.innerWidth;
      MTLG.getStage().canvas.height = bgStage.canvas.height = MTLG.getOptions().height * scaleX;
    } else {
      MTLG.getStage().canvas.width = bgStage.canvas.width = MTLG.getOptions().width * scaleY;
      MTLG.getStage().canvas.height = bgStage.canvas.height = window.innerHeight;
    }

    // scale Background
    bgStage.scaleX = scaleFactor;
    bgStage.scaleY = scaleFactor;
    bgStage.update();

    // scale Menus
    try {
      if (MTLG.menu.isInitialized()) {
        if (scaleX < scaleY) {
          MTLG.menu.getMenuStage().canvas.width = window.innerWidth;
          MTLG.menu.getMenuStage().canvas.height = MTLG.getOptions().height * scaleX;
        } else {
          MTLG.menu.getMenuStage().canvas.width = MTLG.getOptions().width * scaleY;
          MTLG.menu.getMenuStage().canvas.height = window.innerHeight;
        }
        MTLG.menu.resize(scaleFactor);
      }
    } catch (err) {
      MTLG.log("lc", "MTLG.menu scaling error: " + err.message);
    } finally {

    }

    // safe old values
    _scaleOld = scaleFactor;
  }

  // the level could resize itself
  if (_currentLevel) _currentLevel.resize();
  return scaleFactor;
}

MTLG.lc = {
  resizeStage: resizeStage,
  registerLevel: registerLevel,
  registerMenu: registerMenu,
  levelFinished: levelFinished,
  nextScene: nextScene,
  getLevels: getLevels,
  hasMenu: hasMenu,
  goToMenu: goToMenu,
  startGame: startGame,
  goToLogin: goToLogin,
  init: init,
};
MTLG.addModule(init);
